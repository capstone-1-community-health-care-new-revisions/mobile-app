import { initializeApp } from "firebase/app";
import { initializeAuth } from 'firebase/auth';
import { getStorage } from 'firebase/storage';
import { getFirestore } from 'firebase/firestore';
import { getReactNativePersistence } from 'firebase/auth';
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';



// Your web app's Firebase configuration
const firebaseConfig = {
 apiKey: "AIzaSyCits9zzNK2ey8JKu0wPyk5-mAW4hLYtho",
  authDomain: "community-health-care-uop.firebaseapp.com",
  databaseURL: "https://community-health-care-uop-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "community-health-care-uop",
  storageBucket: "community-health-care-uop.appspot.com",
  messagingSenderId: "308174125314",
  appId: "1:308174125314:web:84cf2d48eb95766dbeea1b",
  measurementId: "G-PMHBWMZR2X"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(ReactNativeAsyncStorage)
});
const storage = getStorage(app);
const firestore = getFirestore(app);

export { auth, storage, firestore, app };