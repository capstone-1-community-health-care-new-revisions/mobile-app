import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import { NavigationContainer, useFocusEffect } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Entypo, AntDesign, MaterialIcons } from '@expo/vector-icons';
import HomeScreen from '../screens/HomeScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import ForgotPassword from '../screens/ForgotPassword';
import Guidelines from '../screens/Guidelines';
import AboutUs from '../screens/AboutUs';
import ChatScreen from '../screens/ChatScreen';
import SurveyScreen from '../screens/SurveyScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SettingsScreen from '../screens/SettingsScreen';
import useAuth from '../hooks/useAuth';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const screenOptions = {
  tabBarShowLabel: false,
  headerShown: false,
  tabBarStyle: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    elevation: 10,
    height: 60,
    backgroundColor: '#fff',
  },
};

const ProfileStack = createNativeStackNavigator();

const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      options={({ navigation }) => ({
        title: 'Profile',
        headerRight: () => (
          <TouchableOpacity onPress={() => navigation.navigate('Info')}>
            <MaterialIcons name="info" size={30} color="black" style={{ marginRight: 20 }} />
          </TouchableOpacity>
        ),
      })}
    />
    <ProfileStack.Screen name="Info" component={SettingsScreen} />
    <ProfileStack.Screen name="AboutUs" component={AboutUs} />
    <ProfileStack.Screen name="Guidelines" component={Guidelines} />
  </ProfileStack.Navigator>
);

export default function AppNavigation() {
  const { user } = useAuth();

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        {!user ? (
          <>
            <Stack.Screen name="Welcome" options={{ headerShown: false }} component={WelcomeScreen} />
            <Stack.Screen name="Login" options={{ headerShown: false }} component={LoginScreen} />
            <Stack.Screen name="SignUp" options={{ headerShown: false }} component={SignUpScreen} />
            <Stack.Screen name="ForgotPassword" options={{ headerShown: false }} component={ForgotPassword} />
          </>
        ) : (
          <Stack.Screen
            name="MainTab"
            options={{ headerShown: false }}
            component={MainTab}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const MainTab = () => (
  <Tab.Navigator initialRouteName="Home" screenOptions={screenOptions}>
    <Tab.Screen
      name="Home"
      component={HomeScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <Entypo name="home" size={30} color={focused ? '#16247d' : '#111'} />
        ),
      }}
    />
    <Tab.Screen
      name="Chat"
      component={ChatScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <Entypo name="chat" size={30} color={focused ? '#16247d' : '#111'} />
        ),
      }}
    />
    <Tab.Screen
      name="Survey"
      component={SurveyScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <AntDesign name="form" size={30} color={focused ? '#16247d' : '#111'} />
        ),
      }}
    />
    <Tab.Screen
      name="ProfileScreen"
      component={ProfileStackScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <MaterialIcons name="face" size={30} color={focused ? '#16247d' : '#111'} />
        ),
      }}
    />
  </Tab.Navigator>
);
