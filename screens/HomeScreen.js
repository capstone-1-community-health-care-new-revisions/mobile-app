import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Modal, Linking, ImageBackground, ScrollView, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome';
import categorizedCardData from './CategorizedCardData'; // Import the categorized card data

export default function ChatScreen({ navigation }) {
  const [selectedCard, setSelectedCard] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedCardInfo, setSelectedCardInfo] = useState({});
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredData, setFilteredData] = useState(categorizedCardData);

  const handleCardPress = (category, card) => {
    setSelectedCard(card.id);
    setSelectedCardInfo(card);
    setModalVisible(true);
  };

  const openURL = (url) => {
    Linking.openURL(url);
  };

  const renderCard = ({ item }) => {
    return (
      <View style={styles.cardContainer}>
        <TouchableOpacity
          style={[styles.cardTouchable, { backgroundColor: selectedCard === item.id ? '#AEE8F5' : 'lightblue' }]}
          onPress={() => handleCardPress(item.category, item)}
        >
          <ImageBackground source={item.image} style={styles.imageBackground} resizeMode="cover">
            {/* Title removed from here */}
          </ImageBackground>
        </TouchableOpacity>
        {/* Title placed below the card container */}
        <Text style={styles.title}>{item.title}</Text>
      </View>
    );
  };

  const renderCategory = (category) => {
    return (
      <View style={styles.categoryContainer}>
        <View style={styles.arrowContainer}>
          <Icon name="angle-left" size={24} color="black" />
        </View>
        <View style={styles.categoryContent}>
          <Text style={styles.categoryTitle}>{category}</Text>
          <FlatList
            data={filteredData[category]}
            renderItem={renderCard}
            keyExtractor={(item) => item.id}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={styles.arrowContainer}>
          <Icon name="angle-right" size={24} color="black" />
        </View>
      </View>
    );
  };

  const handleSearch = (query) => {
    setSearchQuery(query);
    const newData = {};

    // Filter categorizedCardData based on search query for both titles and category titles
    Object.keys(categorizedCardData).forEach((category) => {
      const filteredCategory = categorizedCardData[category].filter((item) =>
        item.title.toLowerCase().includes(query.toLowerCase())
      );
      if (category.toLowerCase().includes(query.toLowerCase()) && filteredCategory.length === 0) {
        // If the query matches a category title and no specific items are found, include all items from that category
        newData[category] = categorizedCardData[category];
      } else if (filteredCategory.length > 0) {
        // If specific items are found, include them in the filtered data
        newData[category] = filteredCategory;
      }
    });

    // Update filtered data
    setFilteredData(newData);
  };

  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Search cards..."
        value={searchQuery}
        onChangeText={handleSearch}
      />
      <ScrollView style={styles.scrollView}>
        {Object.keys(filteredData).map((category) => (
          <View key={category}>{renderCategory(category)}</View>
        ))}
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>{selectedCardInfo.title}</Text>
            <Text style={styles.modalDescription}>{selectedCardInfo.description}</Text>
            <TouchableOpacity onPress={() => openURL(selectedCardInfo.url)}>
              <Text style={styles.modalUrl}>{selectedCardInfo.url}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#AEE8F5',
  },
  scrollView: {
    flex: 1,
    marginBottom: 60,
  },
  categoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
    backgroundColor: '#DCF8C6',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  arrowContainer: {
    padding: 5,
  },
  categoryTitle: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  cardContainer: {
    marginVertical: 5,
    marginHorizontal: 10,
    padding: 0,
    borderRadius: 10,
    width: 150,
    height: 200, // Increased to accommodate title below
    justifyContent: 'flex-end',
    overflow: 'hidden',
  },
  cardTouchable: {
    flex: 1,
  },
  imageBackground: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
  },
  title: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    width: '100%',
  },
  modalTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  modalDescription: {
    fontSize: 20,
    marginBottom: 10,
  },
  modalUrl: {
    fontSize: 16,
    color: 'blue',
  },
  categoryContent: {
    flex: 1,
    maxWidth: '89%',
  },
  searchInput: {
    height: 45,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    margin: 10,
    marginTop: 30,
  },
});
