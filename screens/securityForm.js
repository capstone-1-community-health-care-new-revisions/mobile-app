import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView, Alert } from 'react-native';
import { getAuth } from 'firebase/auth';
import { getFirestore, doc, getDoc, onSnapshot } from 'firebase/firestore';

export default function SecurityForm({ onDetailsSubmit }) {
  const [name, setName] = useState('');
  const [studentNumber, setStudentNumber] = useState('');
  const [course, setCourse] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [age, setAge] = useState('');
  const [isFormComplete, setIsFormComplete] = useState(false);

  const auth = getAuth();
  const currentUser = auth.currentUser;

  useEffect(() => {
    if (currentUser) {
      setName(currentUser.displayName || '');

      const fetchStudentNumber = async () => {
        const firestore = getFirestore();
        const userDocRef = doc(firestore, 'users', currentUser.uid);

        try {
          const userDocSnapshot = await getDoc(userDocRef);
          if (userDocSnapshot.exists()) {
            const userData = userDocSnapshot.data();
            if (userData && userData.studentNo) {
              setStudentNumber(userData.studentNo);
              setHeight(userData.height || '');
              setWeight(userData.weight || '');
              setAge(userData.age || '');
              setCourse(userData.course || '');
            }
          }
        } catch (error) {
          console.error('Error fetching user data:', error);
        }
      };

      // Subscribe to real-time updates on the user document
      const unsubscribe = onSnapshot(doc(getFirestore(), 'users', currentUser.uid), (doc) => {
        if (doc.exists()) {
          const userData = doc.data();
          if (userData && userData.studentNo) {
            setStudentNumber(userData.studentNo);
            setHeight(userData.height || '');
            setWeight(userData.weight || '');
            setAge(userData.age || '');
            setCourse(userData.course || '');
          }
        }
      });

      // Show disclaimer message
      const disclaimerTimeout = setTimeout(() => {
        Alert.alert(
          'Disclaimer',
          'The accuracy of the symptoms given is not as accurate as the personal doctor’s recommendation. This application is just for reference that gives insight into your common health issue.',
          [{ text: 'OK' }]
        );
      }, 1000);

      return () => {
        // Unsubscribe from real-time updates when component unmounts
        unsubscribe();
        clearTimeout(disclaimerTimeout);
      };
    }
  }, [currentUser]);

  const handleSubmit = () => {
    onDetailsSubmit({ name, studentNumber, course, height, weight, age });
  };

  useEffect(() => {
    if (name && studentNumber && course && height && weight && age) {
      setIsFormComplete(true);
    } else {
      setIsFormComplete(false);
    }
  }, [name, studentNumber, course, height, weight, age]);

  // Functions to handle input changes and validation (similar to what you already have)

  return (
    <View style={styles.securityFormContainer}>
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.securityForm}>
          <Text style={styles.formHeading}>Student Details Form</Text>
          <View>
            <Text style={styles.label}>Name:</Text>
            <TextInput
              style={styles.input}
              placeholder="LASTNAME, FIRSTNAME M.I"
              value={name}
              onChangeText={setName}
              editable={!name}
            />
            <Text style={styles.label}>Student Number:</Text>
            <TextInput
              style={styles.input}
              placeholder="Student No. 00-0000-000000"
              value={studentNumber}
              onChangeText={setStudentNumber}
              editable={!studentNumber}
            />
            <Text style={styles.label}>Course:</Text>
            <TextInput
              style={styles.input}
              placeholder="Course ex.BSIT"
              value={course}
              onChangeText={setCourse}
            />
            <Text style={styles.label}>Height:</Text>
            <TextInput
              style={styles.input}
              placeholder="Height"
              value={height}
              onChangeText={setHeight}
            />
            <Text style={styles.label}>Weight:</Text>
            <TextInput
              style={styles.input}
              placeholder="Weight"
              value={weight}
              onChangeText={setWeight}
            />
            <Text style={styles.label}>Age:</Text>
            <TextInput
              style={styles.input}
              placeholder="Age"
              value={age}
              onChangeText={setAge}
            />
          </View>
          <TouchableOpacity
            style={[styles.submitButton, { backgroundColor: isFormComplete ? '#70bb85' : '#777777' }]}
            onPress={handleSubmit}
            disabled={!isFormComplete}
          >
            <Text style={styles.submitButtonText}>Submit</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  securityFormContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollViewContent: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  securityForm: {
    backgroundColor: '#f0f0f0',
    padding: 20,
    borderRadius: 10,
    width: 320,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  formHeading: {
    color: 'navy',
    fontSize: 20,
    marginBottom: 10,
  },
  label: {
    marginBottom: 5,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
  },
  input: {
    width: '100%',
    padding: 10,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    marginBottom: 10,
    color: 'black',
  },
  submitButton: {
    backgroundColor: '#777777',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  submitButtonText: {
    color: 'white',
    fontSize: 16,
  },
});
