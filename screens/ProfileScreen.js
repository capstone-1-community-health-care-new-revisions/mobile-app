import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Text, Modal } from 'react-native';
import { Avatar, Input, Button } from 'react-native-elements';
import { FontAwesome } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import { getAuth, updateProfile, onAuthStateChanged } from 'firebase/auth';
import { getFirestore, doc, setDoc, getDoc } from 'firebase/firestore';
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from 'firebase/storage';

const ProfileScreen = () => {
  const auth = getAuth();
  const [user, setUser] = useState(auth.currentUser);
  const [isAuthenticated, setIsAuthenticated] = useState(!!auth.currentUser);
  const [displayName, setDisplayName] = useState('');
  const [profilePhoto, setProfilePhoto] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [yearGradeModalVisible, setYearGradeModalVisible] = useState(false);
  const [selectedYear, setSelectedYear] = useState(user?.yearGrade || '');
  const [studentNo, setStudentNo] = useState('');
  const [selectedDepartment, setSelectedDepartment] = useState('');
  const [departmentModalVisible, setDepartmentModalVisible] = useState(false);
  const [courseModalVisible, setCourseModalVisible] = useState(false);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [isEditableFieldsInitialized, setIsEditableFieldsInitialized] = useState(false);
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [age, setAge] = useState('');
  const firestore = getFirestore();
  const userDocRef = doc(firestore, 'users', user?.uid);
  const storage = getStorage();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (authUser) => {
      if (authUser) {
        setUser(authUser);
        setIsAuthenticated(true);
      } else {
        setUser(null);
        setIsAuthenticated(false);
      }
      resetState();
    });

    return () => unsubscribe();
  }, [auth]);

  useEffect(() => {
    if (user) {
      fetchProfileInfo();
    }
  }, [user]);

  useEffect(() => {
    setEditMode(!isEditableFieldsInitialized);
  }, [isEditableFieldsInitialized]);

  const resetState = () => {
    setDisplayName('');
    setProfilePhoto(null);
    setIsEditing(false);
    setYearGradeModalVisible(false);
    setSelectedYear('');
    setStudentNo('');
    setSelectedDepartment('');
    setDepartmentModalVisible(false);
    setCourseModalVisible(false);
    setSelectedCourse('');
    setIsEditableFieldsInitialized(false);
  };

  const fetchProfileInfo = async () => {
    try {
      const userDoc = await getDoc(userDocRef);
      const userData = userDoc.data();
      if (userData) {
        setDisplayName(userData.displayName || '');
        setProfilePhoto(userData.photoURL || '');
        setSelectedYear(userData.yearGrade || '');
        setStudentNo(userData.studentNo || '');
        setSelectedDepartment(userData.department || '');
        setSelectedCourse(userData.course || ''); 
        setIsEditableFieldsInitialized(!!userData.isEditableFieldsInitialized);
        setHeight(userData.height || '');
        setWeight(userData.weight || '');
        setAge(userData.age || '');
      }
    } catch (error) {
      console.error('Error fetching profile info:', error);
    }
  };

  const handleProfileUpdate = async () => {
    try {
      let downloadURL = profilePhoto;

      if (profilePhoto && profilePhoto.startsWith('file:')) {
        const response = await fetch(profilePhoto);
        const blob = await response.blob();
        const storageRef = ref(storage, `profile_photos/${user.uid}`);
        const uploadTask = uploadBytesResumable(storageRef, blob);

        await uploadTask;

        downloadURL = await getDownloadURL(storageRef);
      }

      await updateProfile(auth.currentUser, {
        displayName,
        photoURL: downloadURL,
      });

      await setDoc(userDocRef, {
        displayName,
        photoURL: downloadURL,
        yearGrade: selectedYear,
        studentNo,
        department: selectedDepartment,
        course: selectedCourse,
        isEditableFieldsInitialized: true,
        height,
        weight,
        age,
      });

      setIsEditing(false);
      setEditMode(true);
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  const pickImage = async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.5,
      });

      if (!result.canceled) {
        setProfilePhoto(result.assets[0].uri);
      }
    } catch (error) {
      console.error('Error picking an image:', error);
    }
  };

  const handleYearGradeSelect = (year) => {
    setSelectedYear(year);
    setYearGradeModalVisible(false);
  };

  const handleDepartmentSelect = (department) => {
    setSelectedDepartment(department);
    setDepartmentModalVisible(false);
  };

  const handleCourseSelect = (course) => {
    setSelectedCourse(course);
    setCourseModalVisible(false);
  };

  const renderYearGradeModal = () => (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <TouchableOpacity onPress={() => handleYearGradeSelect('1st year')}>
          <Text style={styles.modalText}>1st year</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleYearGradeSelect('2nd year')}>
          <Text style={styles.modalText}>2nd year</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleYearGradeSelect('3rd year')}>
          <Text style={styles.modalText}>3rd year</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleYearGradeSelect('4th year')}>
          <Text style={styles.modalText}>4th year</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleYearGradeSelect('5th year')}>
          <Text style={styles.modalText}>5th year</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const renderDepartmentModal = () => (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CEA')}>
          <Text style={styles.modalText}>CEA</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CITE')}>
          <Text style={styles.modalText}>CITE</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CMA')}>
          <Text style={styles.modalText}>CMA</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CELA')}>
          <Text style={styles.modalText}>CELA</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CCJE')}>
          <Text style={styles.modalText}>CCJE</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleDepartmentSelect('CAHS')}>
          <Text style={styles.modalText}>CAHS</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const renderCourseModal = () => (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <ScrollView contentContainerStyle={styles.modalScrollView}> 
          {[
            'BS Criminology',
            'Bachelor of Arts in Communication',
            'Bachelor of Arts in Political Science',
            'Bachelor of Elementary Education',
            'Bachelor of Secondary Education - Science',
            'Bachelor of Secondary Education - Social Studies',
            'Bachelor of Secondary Education - English',
            'BS Accountancy',
            'BS Hospitality Management',
            'BS Tourism Management',
            'BS Management Accounting',
            'BS Accounting Information System',
            'BS Business Administration - Financial Management',
            'BS Business Administration - Marketing Management',
            'BS Information Technology',
            'BS Nursing',
            'BS Pharmacy',
            'BS Psychology',
            'BS Medical Laboratory Science',
            'BS Architecture',
            'BS Civil Engineer',
            'BS Computer Engineering',
            'BS Electronics Engineering',
            'BS Electrical Engineering',
            'BS Mechanical Engineering',
          ].map((course) => (
            <TouchableOpacity key={course} onPress={() => handleCourseSelect(course)}>
              <Text style={styles.modalText}>{course}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.avatarContainer}>
          <Avatar
            rounded
            size="xlarge"
            source={{
              uri: profilePhoto || 'https://example.com/default-profile-image.jpg',
            }}
            onPress={() => (isEditing || editMode) && pickImage()}
          >
            {!profilePhoto && (
              <View style={styles.cameraIconContainer}>
                <FontAwesome
                  name="plus"
                  size={60}
                  color="#666"
                  style={styles.cameraIcon}
                />
              </View>
            )}
          </Avatar>
        </View>

        <View style={styles.inputContainer}>
          <Text style={styles.displayText}>{displayName}</Text>
<Input
  label="Student No."
  value={studentNo}
  onChangeText={setStudentNo}
  placeholder="Enter student number"
  editable={isAuthenticated && isEditing}
  keyboardType="numeric"
  inputStyle={{ color: 'black' }}
/>
          <View>
            <Input
              label="Height"
              value={height}
              onChangeText={(text) => {
                if (/^[0-9']*$/i.test(text)) { 
                  setHeight(text);
                }
              }}
              placeholder="Enter height"
              editable={(isAuthenticated && isEditing) || !editMode}
              keyboardType="visible-password" 
              inputStyle={{ color: 'black' }}
            />

            <Input
              label="Weight"
              value={weight}
              onChangeText={setWeight}
              placeholder="Enter weight"
              editable={(isAuthenticated && isEditing) || !editMode}
              keyboardType="numeric"
              inputStyle={{ color: 'black' }}
            />

            <Input
              label="Age"
              value={age}
              onChangeText={setAge}
              placeholder="Enter age"
              editable={(isAuthenticated && isEditing) || !editMode}
              keyboardType="numeric"
              inputStyle={{ color: 'black' }}
            />
          </View>

          <TouchableOpacity
            onPress={() => (isAuthenticated && isEditing) && setYearGradeModalVisible(true)}
          >
            <View style={styles.pickerButton}>
              <Text>{`Year/Grade: ${selectedYear}`}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => (isAuthenticated && isEditing) && setDepartmentModalVisible(true)}
          >
            <View style={styles.pickerButton}>
              <Text>{`Department: ${selectedDepartment}`}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity 
            onPress={() => (isAuthenticated && isEditing) && setCourseModalVisible(true)}
          >
            <View style={styles.pickerButton}>
              <Text>{`Course: ${selectedCourse}`}</Text>
            </View>
          </TouchableOpacity>

          {!isEditing && (
            <TouchableOpacity onPress={() => setIsEditing(true)}>
              <Text style={styles.editProfileButton}>Edit Profile</Text>
            </TouchableOpacity>
          )}

          {isEditing && (
            <Button
              title="Update"
              onPress={handleProfileUpdate}
              buttonStyle={{
                backgroundColor: isEditing ? 'black' : '#AEE8F5',
                borderRadius: 10,
                marginTop: 50,
              }}
            />
          )}
        </View>

        <Modal
          animationType="slide"
          transparent={true}
          visible={yearGradeModalVisible}
          onRequestClose={() => setYearGradeModalVisible(false)}
        >
          {renderYearGradeModal()}
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={departmentModalVisible}
          onRequestClose={() => setDepartmentModalVisible(false)}
        >
          {renderDepartmentModal()}
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={courseModalVisible}
          onRequestClose={() => setCourseModalVisible(false)}
        >
          {renderCourseModal()}
        </Modal>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  cameraIconContainer: {
    width: 150,
    height: 150,
    borderRadius: 75,
    backgroundColor: '#d3d3d3',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
  },
  cameraIcon: {
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: 'lightblue',
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 110,
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20,
  },
  inputContainer: {
    width: '100%',
  },
  outputContainer: {
    marginTop: 20,
  },
  editProfileButton: {
    color: 'black',
    marginTop: 30,
    textAlign: 'center',
    fontSize: 18,
  },
  pickerButton: {
    backgroundColor: '#AEE8F5',
    padding: 10,
    borderRadius: 5,
    marginTop: 5,
    width: '100%',
  },
  displayText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
    elevation: 5,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  modalScrollView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ProfileScreen;
