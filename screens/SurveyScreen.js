import React, { useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import symptoms from './symptoms';
import symptomToIssueMapping from './symptomToIssueMapping';
import SecurityForm from './securityForm';
import axios from 'axios';
import moment from 'moment-timezone';
import descriptions from './description';

export default function SurveyScreen() {
  const [selectedSymptoms, setSelectedSymptoms] = useState([]);
  const [potentialIssues, setPotentialIssues] = useState([]);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [securityFormDetails, setSecurityFormDetails] = useState(null);
  const [course, setCourse] = useState('');
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [selectedIssue, setSelectedIssue] = useState(null); 

  const handleDetailsSubmit = (details) => {
    setSecurityFormDetails(details);
    setCourse(details.course);
  };

  const handleSymptomChange = (symptom) => {
    if (selectedSymptoms.length === 5 && !selectedSymptoms.includes(symptom)) {
      console.error('Maximum number of symptoms selected.');
      return;
    }

    const updatedSymptoms = selectedSymptoms.includes(symptom)
      ? selectedSymptoms.filter((s) => s !== symptom)
      : [...selectedSymptoms, symptom];
    setSelectedSymptoms(updatedSymptoms);
  };

  const handleSubmit = async () => {
    if (!securityFormDetails) {
      console.error('Please submit security form details first.');
      return;
    }

    if (selectedSymptoms.length < 1) {
      console.error('Please select at least one symptom.');
      return;
    }

    const potentialIssues = getPotentialIssues(selectedSymptoms);
    setPotentialIssues(potentialIssues);

    setShowConfirmation(true);
  };

  const handleSubmissionConfirmed = async () => {
    setShowConfirmation(false);

    const formattedDateTime = moment().tz('Asia/Manila').format('YYYY-MM-DD HH:mm:ss');

    try {
      const response = await axios.post('http://172.20.10.2:2005/api/data', {
        name: securityFormDetails.name,
        studentNumber: securityFormDetails.studentNumber,
        course: securityFormDetails.course,
        height: securityFormDetails.height,
        weight: securityFormDetails.weight,
        age: securityFormDetails.age,
        symptoms: selectedSymptoms,
        potentialIssues: potentialIssues,
        submissionDateTime: formattedDateTime,
      });

      if (response.status === 200) {
        console.log('User data submitted successfully');
        setIsPopupOpen(true);
      } else {
        console.error('Error submitting user data:', response.data.error);
      }
    } catch (error) {
      console.error('Error submitting user data:', error);
    }
  };

  const handleSubmissionCancelled = () => {
    setShowConfirmation(false);
  };

  const handleReturnToForm = () => {
    setIsPopupOpen(false);
    setSelectedSymptoms([]);
    setPotentialIssues([]);
    setSecurityFormDetails(null);
  };

  const getPotentialIssues = (selectedSymptoms) => {
    const potentialIssuesCount = selectedSymptoms.reduce((issuesCount, symptom) => {
      if (symptomToIssueMapping[symptom]) {
        symptomToIssueMapping[symptom].forEach(issue => {
          issuesCount[issue] = (issuesCount[issue] || 0) + 1;
        });
      }
      return issuesCount;
    }, {});

    const sortedIssues = Object.entries(potentialIssuesCount)
      .sort((a, b) => b[1] - a[1])
      .map(issue => issue[0]);

    // Return only 2 or 4 potential issues based on the number of selected symptoms
    return sortedIssues.slice(0, selectedSymptoms.length > 3 ? 4 : 2);
  };

  // Function to handle the press event of each potential issue
  const handleIssuePress = (issue) => {
    setSelectedIssue(issue);
  };

  return (
    <View style={styles.surveyContainer}>
      {securityFormDetails ? (
        <ScrollView style={{ marginTop: 30, marginBottom: 60 }}>
          <Text style={styles.formHeading}>Choose symptoms:</Text>
          {symptoms.map((category) => (
            <View key={category.category}>
              <View style={styles.symptomContainer}>
                <Text style={styles.symptomBoxTitle}>{category.category}</Text>
                {category.symptoms.map((symptom) => (
                  <TouchableOpacity
                    key={symptom}
                    style={[styles.symptomButton, selectedSymptoms.includes(symptom) && styles.selectedSymptomButton]}
                    onPress={() => handleSymptomChange(symptom)}
                  >
                    <Text style={styles.symptomButtonText}>{symptom}</Text>
                  </TouchableOpacity>
                ))}
                <TouchableOpacity
                  style={styles.submitButton}
                  onPress={() => handleSubmit(category.category)}
                >
                  <Text style={styles.submitButtonText}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))}
        </ScrollView>
      ) : (
        <SecurityForm onDetailsSubmit={handleDetailsSubmit} />
      )}

      {showConfirmation && (
        <View style={styles.popup}>
          <View style={styles.popupContent}>
            <Text style={styles.confirmationText}>Are you sure you want to submit with the following symptoms?</Text>
            <ScrollView style={styles.potentialIssuesList}>
              {selectedSymptoms.map((symptom, index) => (
                <Text key={index}>{symptom}</Text>
              ))}
            </ScrollView>
            <View style={styles.confirmationButtons}>
              <TouchableOpacity style={styles.confirmButton} onPress={handleSubmissionConfirmed}>
                <Text style={styles.confirmButtonText}>Yes</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.cancelButton} onPress={handleSubmissionCancelled}>
                <Text style={styles.cancelButtonText}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}

      {isPopupOpen && (
        <View style={styles.popup}>
          <View style={styles.popupContent}>
            <TouchableOpacity style={styles.close} onPress={handleReturnToForm}>
              <Text style={styles.closeText}>&times;</Text>
            </TouchableOpacity>
            <Text style={styles.potentialIssuesHeading}>Potential Health Issues:</Text>
            <ScrollView style={styles.potentialIssuesList}>
              {potentialIssues.map((issue, index) => (
                <TouchableOpacity
  key={index}
  onPress={() => handleIssuePress(issue)}
  style={[styles.potentialIssuesList, selectedIssue === issue && styles.selectedPotentialIssue]}
>
  <Text>{issue}</Text>
</TouchableOpacity>
              ))}
            </ScrollView>
            {selectedIssue && potentialIssues.includes(selectedIssue) && (
  <Text style={styles.issueDescription}>{descriptions[selectedIssue]}</Text>
)}
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  surveyContainer: {
    flex: 1,
    backgroundColor: 'lightblue',
    padding: 20,
  },
  formHeading: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  symptomContainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    marginBottom: 20,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  symptomBoxTitle: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  symptomButton: {
    borderWidth: 2,
    borderColor: '#000',
    borderRadius: 5,
    padding: 8,
    marginRight: 8,
    marginBottom: 8,
  },
  selectedSymptomButton: {
    backgroundColor: '#3CB371',
  },
  symptomButtonText: {
    fontSize: 16,
  },
  submitButton: {
    backgroundColor: '#3CB371',
    padding: 20,
    width: 200,
    borderRadius: 10,
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  submitButtonText: {
    color: 'white',
    fontSize: 16,
  },
  popup: {
    position: 'absolute',
    top: 0,
    left: 20,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedPotentialIssue: {
  backgroundColor: 'lightgreen',
  padding: 5,
  marginVertical: 5,
  borderRadius: 5,
},
  popupContent: {
    backgroundColor: 'white',
    padding: 25,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  close: {
    position: 'absolute',
    top: 10,
    right: 10,
    fontSize: 24,
  },
  closeText: {
    fontSize: 30,
    color: 'black',
  },
  potentialIssuesHeading: {
    fontSize: 30,
    marginBottom: 15,
  },
  potentialIssuesList: {
    maxHeight: 150,
  },
  confirmationText: {
    fontSize: 18,
    marginBottom: 10,
  },
  confirmationButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  confirmButton: {
    backgroundColor: '#3CB371',
    padding: 10,
    borderRadius: 5,
  },
  confirmButtonText: {
    color: 'white',
    fontSize: 16,
  },
  cancelButton: {
    backgroundColor: 'gray',
    padding: 10,
    borderRadius: 5,
  },
  cancelButtonText: {
    color: 'white',
    fontSize: 14,
  },
});
