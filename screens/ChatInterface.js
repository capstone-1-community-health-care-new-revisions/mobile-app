import React, { useState, useEffect, useRef } from 'react';
import { View, Text, TextInput, Image, SafeAreaView, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { collection, addDoc, serverTimestamp, onSnapshot, query, orderBy, doc, setDoc } from 'firebase/firestore';
import { firestore } from '../config/firebase';
import { format } from 'date-fns';
import Icon from 'react-native-vector-icons/FontAwesome';

const ChatInterface = ({ conversation, currentUser }) => {
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const scrollViewRef = useRef();

  const sendMessage = async () => {
  try {
    if (!message.trim()) {
      return;
    }

    const newMessage = {
      text: message,
      senderId: currentUser.id,
      displayName: currentUser.displayName,
      photoURL: currentUser.photoURL,
      timestamp: serverTimestamp()
    };

    // Add message to the main conversation collection
    await addDoc(collection(firestore, 'conversations', conversation.conversationId, 'messages'), newMessage);

    // Update the lastMessages subcollection within the conversation document
    const conversationRef = doc(firestore, 'conversations', conversation.conversationId);
    await setDoc(conversationRef, { lastMessage: newMessage }, { merge: true });

    setMessage('');
  } catch (error) {
    console.error('Error sending message:', error);
  }
};


  useEffect(() => {
    const unsubscribe = onSnapshot(
      query(collection(firestore, 'conversations', conversation.conversationId, 'messages'), orderBy('timestamp')),
      (snapshot) => {
        const newMessages = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data()
        }));
        setMessages(newMessages);

        if (scrollViewRef.current) {
          scrollViewRef.current.scrollToEnd({ animated: true });
        }
      },
      (error) => {
        console.error('Error fetching messages:', error);
      }
    );

    return () => unsubscribe();
  }, [conversation]);

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <Text style={styles.text1}>Chat</Text>
      <View style={styles.container}>
        <ScrollView
          ref={scrollViewRef}
          contentContainerStyle={styles.scrollContent}
          onContentSizeChange={() => {
            if (scrollViewRef.current) {
              scrollViewRef.current.scrollToEnd({ animated: true });
            }
          }}
        >
          {messages.map((msg) => (
            <View key={msg.id} style={[styles.messageWrapper, { flexDirection: msg.senderId === currentUser.id ? 'row-reverse' : 'row', alignItems: 'center' }]}>
              <Image
                source={{ uri: msg.photoURL }}
                style={styles.avatar}
              />
              <View style={[styles.messageBubble, { backgroundColor: msg.senderId === currentUser.id ? '#DCF8C6' : 'lightgray' }]}>
                <Text style={msg.senderId === currentUser.id ? styles.boldText : styles.boldText}>
                  {msg.displayName}
                </Text>
                <Text>{msg.text}</Text>
                <Text style={styles.boldText1}>{msg.timestamp ? format(msg.timestamp.toDate(), 'h:mm aa') : 'Invalid Timestamp'}</Text>
              </View>
            </View>
          ))}
        </ScrollView>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Type your message..."
            value={message}
            onChangeText={setMessage}
          />
          <TouchableOpacity onPress={sendMessage}>
            <Icon name="send" size={30} color="black" />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: 'rgba(173, 216, 230, 0.945)',
  },
  container: {
    flex: 1,
  },
  scrollContent: {
    flexGrow: 1,
    padding: 10,
    paddingBottom: 120
  },
  messageWrapper: {
    alignItems: 'center',
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 5,
  },
  messageBubble: {
    padding: 10,
    borderRadius: 10,
    margin: 5,
  },
  inputContainer: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    position: 'absolute',
    bottom: 60,
    left: 0,
    right: 0,
    alignItems: 'center',
    height: 60,
  },
  input: {
    flex: 1,
    padding: 10,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
    height: '100%',
  },
  text1: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 40,
  },
  messageBubble: {
    padding: 10,
    borderRadius: 10,
    marginVertical: 5, 
    marginHorizontal: 10, 
    alignSelf: 'flex-start', 
    maxWidth: '80%', 
  },
  boldText: {
    fontWeight: '700',
  },
  boldText1: {
    color:'#555',
  },
  normalText: {
    fontWeight: 'normal',
  },
});

export default ChatInterface;
