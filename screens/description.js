const descriptions = {
    'Externa otitis': `Otitis externa is the most common hearing disorder and consists of an inflammation of the outer part of the ear. It is caused by a bacterial or fungal (fungal) infection of the external ear canal.

It is usually caused by swimming in water contaminated by these pathogens, which reach the ears when the person is submerged in the water. The main symptomatology is earache, although redness of the ear and swelling of the lymph nodes around it are also common. Fever and hearing loss are not common.
Treatment consists of applying antibiotic ear drops, which are applied for a week until the infection subsides.`,

    'Acute otitis media': `Acute otitis media consists of an infection of the middle ear, located behind the eardrum, by bacteria or viruses. It is caused by a blockage of the Eustachian tube, which is responsible for draining fluid, but if it becomes clogged, it can lead to the growth of pathogens that will lead to infection.
Being acute, this otitis media consists of a short episode but with a lot of earache. The symptoms are similar to external otitis, although here the pain is greater. The problem with otitis media is that the causative germs can spread to other structures in the head, so it is important to treat it quickly.
To avoid causing hearing problems, otitis media is treated in the same way as external, with the application of antibiotic ear drops.`,

    'Secretory otitis media': `Secretory otitis media develops when acute otitis media has not fully resolved, so there is still an excess of fluid in the middle ear.
The main symptomatology is that there is a certain loss of hearing due to the plugging of the Eustachian tubes, which hinder the movement of the eardrum, so it does not capture vibrations well. In addition, those affected often have a feeling of congestion in the ear and notice clicking sounds when swallowing.
Treatment consists of applying decongestants and performing maneuvers to regain pressure in the ear, as the plugging makes it too low. If this doesn't resolve, an ear may need to be drained.`,

    'Ménière\'s disease': `Ménière's disease is a disorder of the inner ear caused by the accumulation of fluid in the inner ear, although it is not known what causes this to occur.
This condition is characterized by episodes of vertigo and dizziness. In addition, it can cause hearing loss, a feeling of plugging, the perception of ringing in the ears, etc.
There is no cure for this disease, so treatments (medications to prevent dizziness and nausea) are aimed at reducing the severity of symptoms.`,

    'Vestibular neuritis': `Vestibular neuritis consists of an inflammation of the vestibular nerve, which is located in the inner ear and is responsible for controlling balance.
This inflammation is caused by the infection of a virus and the symptoms usually consist of a crisis of vertigo that lasts between 7 and 10 days. This bout of dizziness may be accompanied by nausea, vomiting, and rapid twitching of the eyes from nerve damage.
Being caused by a virus, it cannot be treated with antibiotics. Treatment consists of relieving the symptoms of vertigo and dizziness, as well as giving intravenous fluids to avoid dehydration if vomiting is very frequent.`,

    'Presbycusis': `Presbycusis is the gradual loss of hearing. It is very common for it to appear with age. In fact, a third of people over 65 have hearing loss.
This disorder is caused by aging itself, although the lifestyle that the person has led has a great influence. Hearing loss is never total, although symptoms include: difficulty in carrying on a conversation, trouble picking up soft sounds, muffling of speech, asking people to speak slowly, etc. In short, it compromises the sociability of the person.
The damage to the ears is irreversible, so the lost hearing cannot be recovered. Treatment consists of applying hearing aids, devices that are placed in the ear and that amplify sounds.`,

    'Tinnitus': `Tinnitus (or tinnitus) is an auditory disorder characterized by the recurrent perception of noise or buzzing in the ear. It is very common, since it affects more or less recurrently 20% of the population.
The causes are extremely varied, although they are generally related to disorders of the inner ear. Many times the origin is unknown. The main symptom is that the person hears noise or humming even though there is no sound around them.
Although it is not serious, tinnitus can be very annoying and compromise the quality of life of those affected, especially if the episodes are very recurrent and / or also occur at night, in which case there are usually problems sleeping.
Treatment consists of solving the trigger that has led to tinnitus (for example a wax plug), although if this cannot be done, the doctor may recommend the use of devices that inhibit noise, such as hearing aids or white noise machines.`,

    'Barotrauma of the ear': `A barotrauma is damage to the ear when the body you experience very sudden pressure changes especially when traveling by plane or diving.
The ear is very sensitive to these pressure variations. Symptoms, which usually go away quickly, are as follows: pain, ear plugging, dizziness, and sometimes hearing loss.
There is no treatment, as it is the body's response to pressure changes. Yawning or chewing gum can prevent symptoms from appearing.`,

    'Otosclerosis': `Otosclerosis is an abnormal growth of the bones of the middle ear. The cause is unknown, although it is believed that it could be hereditary.
The symptoms of this bone malformation are the following: progressive hearing loss, dizziness, dizziness, tinnitus, etc. Otosclerosis slowly worsens, but hearing loss can be significant.
Being genetic, there is no cure. Calcium or vitamin D treatments can slow down hearing loss, although this is not entirely substantiated. When the disease has progressed a lot, hearing aids and even surgery on the affected bones (replacing them with a prosthesis) can be helpful.`,

    'Perichondritis': `Perichondritis is an infection of the epithelial tissue that surrounds the cartilage of the ears.. It is usually caused by bacteria of the genus "Pseudomonas", which manage to grow when there are traumatic lesions in the ear that compromise the structure of the perichondrium, which is the layer of skin above the cartilage.
Symptoms include: pain, inflammation and redness of the ear and, occasionally, fever and even suppurations from the wound site.
Treatment consists of antibiotics, although if too much pus collects, drainage surgery may be necessary.`,

    'Osteoma': `An osteoma is a benign tumor (not cancer) that occurs in any type of bone in the body.. They are not a health hazard nor do they spread to other organs. They always stay in the same place.
Despite being more common in other bones of the body, osteomas can appear on the eardrum bone. This causes hearing loss, an increased likelihood of ear infections, and earache.
The tumors are usually very small and not too much of a problem, although if they are larger than normal and severely compromise hearing, surgery may be necessary.`,

    'Acoustic trauma': `Acoustic trauma is injury to the inner ear due to exposure to very loud noise.. It is a very common cause of deafness since the eardrum is very sensitive to vibrations greater than what it can withstand.
The main symptomatology is hearing loss, although tinnitus is also very common. The damage is irreversible, so treatment is only applied if the damage to the eardrum is extremely large and surgery is required.`,

    'Earwax plugs': `There are glands in the ear that produce wax, which protects the ear from irritation from water and dust and pathogens. However, some people produce more than normal and this wax can harden and block the ear canal, forming a wax plug.
Failure to remove excess wax can lead to earache, a feeling of plugging, tinnitus, and even hearing loss. Treatment can be administered at home and consists of applying drops, although if the problem persists, a doctor can wash to remove excess earwax.`,

    'Othematoma': `Auditory exostosis is an ear disorder that appears from prolonged exposure to cold water. Therefore, it is a very common condition in surfers.
Exostosis is characterized by the formation of protrusions in the temporal bone of the skull, a circumstance that can obstruct the ear canal and make it more prone to otitis and other ear diseases.
The treatment is surgical, so it is recommended to prevent the development of this disorder using ear plugs when it is going to come into contact with cold water repeatedly.`,

    'Seborrheic dermatitis': `Seborrheic dermatitis is a fairly common skin disease caused by a fungal infection. (by fungi), although sometimes it is due to a malfunction of the immune system. Despite being more common on the scalp, face and nose, seborrheic dermatitis can also affect the skin of the ears.
Symptoms include redness and itching, which can be very annoying. There is no hearing loss as it does not affect the internal canals of the ear. In addition, it usually disappears without the need for treatment. Personal hygiene is the best way to prevent its appearance.`,

'Myopia': `Nearsightedness or myopia is basically the opposite of hyperopia. Both conditions are refractive errors, but individuals with myopia see closer objects very clearly while objects at a distance are seen as faded and blurred.`,

  'Hyperopia': `Otherwise known as farsightedness, and the opposite of myopia, though both are refractive errors. Hyperopia causes the eye to be unable to properly refract light to focus on images. As such, objects from a distance look clear, but closer objects appear blurred or faded.`,

  'Astigmatism': `Astigmatism occurs when there are imperfections pertaining to the cornea or lens of the eye. Normal eyes have cornea and lens that are equally smooth and proportional, which allows the focus of light rays to travel to the retina. Uncorrected astigmatism, especially in adults, will begin to reflect once patients notice a decrease in vision quality. Astigmatism is sometimes coupled with or is related to a refractive error.`,

  'Cataract': `Cataracts cause the lens of the eye to become cloudy. When trying to focus on images, things may appear to be hazy, blurry, less colorful, and harder to see clearly. People affected by a cataract compare their vision to that of looking through a dusty windshield of a car, or through a very foggy mirror.`,

  'Blepharitis': `Blepharitis refers to the eyelids inflammation. They may appear swollen, red, and produce a burning or sore feeling. At times, a crust forms at the base of the eyelashes. Blepharitis is a common eye disease, especially for people that have dandruff and oily skin.`,

  'Retinal Detachment': `Occurs when the retina lifts and moves away from the back of the eye. As a result, the retina fails to work as intended, thus causing vision to become blurry. Seek immediate medical attention if you have a detached retina.`,

  'Lazy Eye': `Amblyopia or lazy eye refers to an eye disease that causes reduced vision in one eye due to abnormalities in development during early stages in ones life. It generally develops from birth up to seven years of age. Lazy eye rarely affects both eyes.`,

  'Stringy white mucus': `It may form under the lower eyelid or inside the eye. This type of discharge is a possible symptom of allergic conjunctivitis, which is the eyes response to allergens that form from time to time.`,

  'Thick green or gray mucus': `It is a discharge with a greenish or grayish color that can be a symptom of an infection, leading to bacterial conjunctivitis. Upon waking up, your eyes might be hard to open and shut. It comes with symptoms of eye irritation and redness.`,

  'Thick crusty mucus': `On the list of causes of eye discharge is blepharitis, which can have an underlying bacterial infection affecting the eyelids and glands. The thick crusty eye discharge forms like dandruff that thicken the eyelids. It also causes redness and inflammation.`,

  'Watery eyes': `It shows a possible complication of viral conjunctivitis. It comes with various symptoms such as blurred vision, redness, and eyelid swelling. In some cases, it is associated with upper respiratory viral illnesses causing an excessive watery eye discharge.`,

  'White or yellow ball mucus': `It is a watery discharge with white or yellow mucus balls, a common symptom of an infection in the tear drainage system called dacryocystitis. Other symptoms may also manifest, such as facial pain, redness, and swelling around the eyelids. Early treatment may help to prevent serious illnesses.`,
   
   'Tinea capitis': `Tinea capitis or scalp ringworm is a fungal infection that affects your child’s scalp and hair. Symptoms of tinea capitis include swollen red patches, dry scaly rashes, itchiness and hair loss. Mold-like fungi called dermatophytes cause tinea capitis. Treatment for a tinea capitis infection involves the use of an oral antifungal medication.`,

  'Alopecia areata': `Alopecia areata is an autoimmune disease that causes patchy hair loss anywhere on your body, but it most commonly affects the hair on the skin that covers your head (scalp). “Alopecia” is a medical term for hair loss or baldness, and “areata” means that it occurs in small, random areas.`,

  'Trichotillomania': `Trichotillomania, also called hair-pulling disorder, is a mental health condition. It involves frequent, repeated and irresistible urges to pull out hair from your scalp, eyebrows or other areas of your body. You may try to resist the urges, but you can't stop. Trichotillomania is part of a group of conditions known as body-focused repetitive behaviors.`,

  'Head lice': `Head lice are tiny insects that feed on blood from the human scalp. Head lice most often affect children. The insects usually spread through direct transfer from the hair of one person to the hair of another.`,

  'Seborrheic dermatitis': `Seborrheic dermatitis is a common skin condition that mainly affects your scalp. It causes scaly patches, inflamed skin and stubborn dandruff. It usually affects oily areas of the body, such as the face, sides of the nose, eyebrows, ears, eyelids and chest. This condition can be irritating but it's not contagious, and it doesn't cause permanent hair loss.`,

  'Contact dermatitis': `Contact dermatitis is an itchy rash caused by direct contact with a substance or an allergic reaction to it. The rash isn't contagious, but it can be very uncomfortable.`,

  'Acne': `Acne is a skin condition that occurs when your hair follicles become plugged with oil and dead skin cells. It causes whiteheads, blackheads, or pimples. Acne is most common among teenagers, though it affects people of all ages.
  Effective acne treatments are available, but acne can be persistent. The pimples and bumps heal slowly, and when one begins to go away, others seem to crop up.
  Depending on its severity, acne can cause emotional distress and scar the skin. The earlier you start treatment, the lower your risk of such problems.`,

  'Eczema': `An eczema rash can pop up anywhere on the body. But eczema on the face can be especially uncomfortable, painful, and itchy. That's because facial skin is especially sensitive. The rash, which is red, dry, and flaky, can also even blister.
  Facial eczema can appear on its own or alongside eczema on the body. And while some people have it only occasionally, others deal with it on a more consistent basis.
  This article explains the types and causes of facial eczema and how the condition is diagnosed and treated.`,

  'Psoriasis': `Facial psoriasis is psoriasis that happens on the face—no surprise there. Characterized by an overactive immune system that causes skin cells to grow at a rapid pace, it leads to thick, red, and/or scaly patches.`,

  'Melasma': `Melasma is a skin condition characterized by brown or blue-gray patches or freckle-like spots. It’s often called the “mask of pregnancy.” Melasma happens because of overproduction of the cells that make the color of your skin. It is common, harmless and some treatments may help. Melasma usually fades after a few months.`,

  'Rosacea': `Rosacea is a common skin condition that causes flushing or long-term redness on your face. It also may cause enlarged blood vessels and small, pus-filled bumps. Some symptoms may flare for weeks to months and then go away for a while.`,

  'Seborrheic dermatitis': `Seborrheic dermatitis is a common skin condition that mainly affects your scalp. It causes scaly patches, inflamed skin and stubborn dandruff. It usually affects oily areas of the body, such as the face, sides of the nose, eyebrows, ears, eyelids and chest. This condition can be irritating but it's not contagious, and it doesn't cause permanent hair loss.`,

  'Contact dermatitis': `Contact dermatitis is an itchy rash caused by direct contact with a substance or an allergic reaction to it. The rash isn't contagious, but it can be very uncomfortable.`,

  'Migraine': `Migraine is a primary headache disorder, in most cases episodic, that usually lasts 4–72 hours, accompanied by nausea, vomiting and/or photophobia and phonophobia. It is sometimes preceded by a short-lasting aura of unilateral, reversible visual, sensory, or other symptoms.
  Migraine most often begins at puberty and generally affects those aged between 35 and 45 years. It is more common in women, possibly because of hormonal influences. Children typically experience migraine of shorter duration and abdominal symptoms are usually more prominent.
  The exact cause of migraine is currently unknown but it is thought to result from the release of pain-producing inflammatory substances around the nerves and blood vessels of the head. It can be triggered by alcohol and certain foods.`,

  'Cluster Headache': `A cluster headache is pain on one side of your head that lasts from 15 minutes up to three hours. The pain occurs daily for weeks to months, often happening at the same time each day and up to eight times per day. When you feel recurring cluster headache pain, it’s called an attack. After an attack, you may go months or even years before you experience another cluster headache.`,

  'Sinus headaches': `Sinus headaches are headaches that may feel like an infection in the sinuses (sinusitis). You may feel pressure around the eyes, cheeks, and forehead. Perhaps your head throbs. But, this pain might actually be caused by a migraine.`,

  'Tension headaches': `A tension-type headache causes mild to moderate pain that's often described as feeling like a tight band around the head. A tension-type headache is the most common type of headache, yet its causes aren't well understood.
  Treatments are available. Managing a tension-type headache is often a balance between practicing healthy habits, finding effective non-medicine treatments, and using medicines appropriately.`,

  'The temporomandibular (TMJ) joint': `The temporomandibular (TMJ) joint acts like a sliding hinge, connecting your jawbone to your skull. You have one joint on each side of your jaw. TMJ disorders — a type of temporomandibular disorder or TMD — can cause pain in your jaw joint and in the muscles that control jaw movement.
  The exact cause of a person's TMJ disorder is often difficult to determine. Your pain may be due to a combination of factors, such as genetics, arthritis, or jaw injury. Some people who have jaw pain also tend to clench or grind their teeth (bruxism), although many people habitually clench or grind their teeth and never develop TMJ disorders.
  In most cases, the pain and discomfort associated with TMJ disorders are temporary and can be relieved with self-managed care or nonsurgical treatments. Surgery is typically a last resort after conservative measures have failed, but some people with TMJ disorders may benefit from surgical treatments.`,

  'Chapped Lips': `Chapped lips are the result of dry, cracked skin on your lips often due to cold or dry weather, sun exposure, frequently licking your lips, or dehydration. You can treat chapped lips at home with the use of lip balm or ointment to ease any discomfort.`,

  'Oral lichen planus': `Oral lichen planus is an ongoing (chronic) inflammatory condition that affects mucous membranes inside your mouth. It may appear as white, lacy patches; red, swollen tissues; or open sores. These lesions may cause burning, pain, or other discomfort.
  Oral lichen planus can't be passed from one person to another. The disorder occurs when the immune system mounts an attack against cells of the oral mucous membranes for unknown reasons.
  Symptoms can usually be managed, but people who have oral lichen planus need regular monitoring because they may be at risk of developing mouth cancer in the affected areas.`,

  'Cold sores': `Cold sores, or fever blisters, are a common viral infection. They are tiny, fluid-filled blisters on and around the lips, often grouped together in patches. After the blisters break, a scab forms that can last several days. Cold sores usually heal in 2 to 3 weeks without leaving a scar.
  Cold sores spread from person to person by close contact, such as kissing. They're usually caused by herpes simplex virus type 1 (HSV-1), and less commonly herpes simplex virus type 2 (HSV-2). Both of these viruses can affect the mouth or genitals and can be spread by oral sex. The virus can spread even if you don't see the sores.
  There's no cure for cold sores, but treatment can help manage outbreaks. Prescription antiviral medicine or creams can help sores heal more quickly. And they may make future outbreaks happen less often and be shorter and less serious.`,

  'Cavities and tooth decay': `Cavities are areas in the hard surface of your teeth that are damaged. These areas of tooth decay become tiny openings or holes that can lead to a serious toothache, infection, and tooth loss. There are several causes of cavities, including bacteria in your mouth, snacking a lot, sipping sugary drinks, and not cleaning your teeth well.
  Cavities and tooth decay are among the world's most common health problems. They're especially common in children, teenagers, and older adults. But anyone who has teeth can get cavities, including babies.
  If cavities aren't treated, they get larger and affect deeper layers of your teeth. Regular dental visits and good brushing and flossing habits are the best ways to protect your teeth from cavities.`,

  'Tooth Sensitivity': `When you have sensitive teeth, certain activities, such as brushing, eating, and drinking, can cause sharp, temporary pain in your teeth. Sensitive teeth are typically the result of worn tooth enamel or exposed tooth roots. Sometimes, however, tooth discomfort is caused by other factors, such as a cavity, a cracked or chipped tooth, a worn filling, or gum disease.`,

  'Overbite': `An overbite is a type of malocclusion (“bad bite”). It occurs when your upper teeth overlap your lower teeth more than they should.
  A small overbite is normal. In fact, is necessary for proper health and function. But if your upper teeth overlap your lower teeth more than 2 to 4 millimeters, its an overbite — and it could cause jaw pain, tooth erosion, and other oral health conditions.`,
  'Mouth ulcers': `Mouth ulcers are small sores that form on your gums, lips, tongue, inner cheeks, or roof of your mouth. Lots of different things can cause them, including minor injuries, hormonal changes, and emotional stress. Many mouth ulcers go away on their own. Others may require treatment.`,

  'Oral cancer': `Oral cancer includes cancers of the mouth and the back of the throat. Oral cancers develop on the tongue, on the tissue lining the mouth and gums, under the tongue, at the base of the tongue, and the area of the throat at the back of the mouth.`,

  'Halitosis (Bad Breath)': `Halitosis, commonly known as bad breath, is mostly caused by sulphur-producing bacteria that normally live on the surface of the tongue and in the throat. Sometimes, these bacteria start to break down proteins at a very high rate, and odorous volatile sulphur compounds (VSC) are released from the back of the tongue and throat. Halitosis is not infectious. About 2.4% of the adult population suffers from bad breath.`,

  'Oral herpes': `Oral herpes is an infection of the lips, mouth, or gums due to the herpes simplex virus. It causes small, painful blisters commonly called cold sores or fever blisters. Oral herpes is also called herpes labialis.`,

   'Oral thrush': `Oral thrush, also called oral candidiasis, is a condition in which the fungus Candida albicans accumulates on the lining of your mouth. Candida is a normal organism in your mouth, but sometimes it can overgrow and cause symptoms. Oral thrush causes creamy white lesions, usually on your tongue or inner cheeks. Sometimes oral thrush may spread to the roof of your mouth, your gums or tonsils, or the back of your throat.`,

  'Geographic tongue': `Geographic tongue is an inflammatory but harmless condition affecting the surface of the tongue. The tongue usually is covered with tiny, pinkish-white bumps called papillae. These papillae are actually fine, hairlike structures. With geographic tongue, patches on the surface of the tongue are missing papillae. These patches are smooth and red, often with slightly raised borders. Although geographic tongue may look alarming, it does not cause health issues. It's not related to infection or cancer. Geographic tongue sometimes can cause tongue pain and make you more sensitive to certain foods, such as spices, salt, and even sweets.`,

  'Black hairy tongue': `Black hairy tongue is a condition of the tongue that gives it a dark, furry look. The look usually results from a buildup of dead skin cells on the many tiny, rounded bumps on the surface of the tongue. These bumps, called papillae, contain taste buds. When these bumps become longer than usual, they can easily trap and be stained by tobacco, food, drinks, bacteria, or yeast, or other substances. Black hairy tongue may look alarming, but it's usually painless and doesn't cause any health problems. The condition usually goes away by dealing with the causes and by regular mouth and tongue cleaning.`,

   'Stiff neck': `Neck stiffness is almost always a temporary symptom of overusing your neck or sleeping in an unusual position. But it can also be a symptom of meningitis, a dangerous infection that needs treatment right away. Visit a healthcare provider if your stiff neck doesn’t get better on its own in a few days or after you use at-home treatment options.`,

  'Text neck': `Text neck describes a repetitive stress injury or overuse syndrome in the neck, caused by prolonged use of mobile devices with the head bent downward and not moving. Also called tech neck, text neck is commonly associated with texting, but it can be related to many activities performed on phones and tablets while looking downward, such as surfing the web, playing games, or doing work.`,

  'Whiplash': `Whiplash is a neck injury due to forceful, rapid back-and-forth movement of the neck, like the cracking of a whip. Whiplash is commonly caused by rear-end car crashes. But whiplash also can result from sports accidents, physical abuse, and other types of traumas, such as a fall. Whiplash may be called a neck sprain or strain, but these terms also include other types of neck injuries. Most people with whiplash get better within a few weeks by following a treatment plan that includes pain medicine and exercise. However, some people have long-lasting neck pain and other complications.`,
   
  'Dysphagia': `Dysphagia is the medical term to describe difficulty in swallowing. This includes problems with sucking, swallowing, drinking, chewing, eating, dribbling saliva, and closing your lips. Eating and drinking is a vital part of life. Difficulty swallowing can limit what you can eat and drink, leading to frustration, stress, and health problems.`,

  'Sore Throat': `A sore throat, or pharyngitis, is when your throat is red, swollen, and painful, especially when you swallow. It happens when the back of the throat, called the pharynx, is inflamed.`,

  'Strep Throat': `Most sore throats are caused by viruses, but some are caused by bacteria called Streptococcus pyogenes. Streptococcus pyogenes can be spread from person to person very easily. You can catch strep throat through contact with droplets, which are made when an infected person talks, coughs, or sneezes.`,

  'Tonsillitis': `Tonsillitis is inflammation of your tonsils. The tonsils are glands found at both sides of the back of your throat. Your tonsils are part of your immune system and help protect your body against infection. Tonsillitis makes your tonsils swollen and sore. It is most common in children. Adults can get tonsillitis, but it is not common.`,

  'Tonsil stones': `Tonsil stones, or 'tonsilloliths', are small lumps of hardened material that can form in your tonsils. They can cause bad breath and most commonly occur in teenagers. The stones are usually small — it is rare to have a large tonsil stone. Tonsil stones are not harmful and may not need to be treated, but surgical options are available if they become a problem.`,

  'Mononucleosis': `Infectious mononucleosis (mono) is often called the kissing disease. The virus that causes mono (Epstein-Barr virus) is spread through saliva. You can get it through kissing, but you can also be exposed by sharing a glass or food utensils with someone who has mono. However, mononucleosis isn't as contagious as some infections, such as the common cold. If you have mononucleosis, it's important to be careful of certain complications such as an enlarged spleen. Rest and enough fluids are keys to recovery.`,

  'Wet cough': `A wet cough is also referred to as a productive cough because it commonly involves bringing up mucus from the lungs. If you are experiencing a wet cough, it may feel like there’s an object in the back of your throat. It may also feel like something is dripping into your throat or chest. Some of your coughs may bring mucus up into your mouth.`,

  'Dry cough': `A dry cough can have many causes, from allergies to viruses. Home remedies and medications may help provide relief. Drinking plenty of water and avoiding allergens or environmental irritants can also be beneficial.`,

    'Allergic Rhinitis': `People with allergic rhinitis might cough, have watery eyes, and sneeze when they breathe in something they are allergic to. Common allergens include animal dander, dust, and pollen. Allergic rhinitis is sometimes known as hay fever or seasonal allergies if pollen is the cause.`,

  'Deviated Septum': `The nasal cavities are generally equal in size. In contrast, a deviated septum occurs if the septum, or the bone and cartilage separating each nasal cavity, is off-center.`,

  'Nasal Polyps': `These are noncancerous (benign) soft growths of tissue in your nasal passages or sinuses. The growths might not cause symptoms unless they are large and block your nasal passages. You might frequently have sinus infections or feel like you have a head cold.`,

  'Sinusitis': `This is inflammation of your sinuses. Acute sinusitis, often the result of a cold, typically lasts four weeks or less. You might have chronic sinusitis if your symptoms persist for three months or longer. Subacute sinusitis involves symptoms that last one to three months.`,
};

export default descriptions;
