import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import React, { useState, useEffect } from 'react'
import { themeColors } from '../theme'
import { SafeAreaView } from 'react-native-safe-area-context'
import { ArrowLeftIcon } from 'react-native-heroicons/solid';
import { useNavigation } from '@react-navigation/native';
import { createUserWithEmailAndPassword, sendEmailVerification } from 'firebase/auth';
import { auth } from '../config/firebase';
import { getFirestore, doc, setDoc } from 'firebase/firestore';

export default function SignUpScreen() {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [fullName, setName] = useState('');
  const [passwordsMatch, setPasswordsMatch] = useState(true);
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const [confirmPassword, setConfirmPassword] = useState('');
  const [emailVerified, setEmailVerified] = useState(false);

  const handlePasswordChange = (value) => {
    setPassword(value);
    setPasswordsMatch(value === confirmPassword);
  };

  const handleConfirmPasswordChange = (value) => {
    setConfirmPassword(value);
    setPasswordsMatch(value === password);
  };

  const isValidPhinmaEmail = (email) => {
    return email.toLowerCase().endsWith("@phinmaed.com");
  };

  const handleSubmit = async () => {
  if (email && password && passwordsMatch && fullName && isValidPhinmaEmail(email)) {
    try {
      await createUserWithEmailAndPassword(auth, email, password);

      
      const user = auth.currentUser;
      await sendEmailVerification(user);

      
      const firestore = getFirestore();
      const userDocRef = doc(firestore, 'users', user.uid);

      await setDoc(userDocRef, {
        displayName: fullName,
      });

      
      await auth.signOut();

      
      alert('Verification email sent. Please check your email to verify your account.');

    } catch (err) {
      console.log('got error: ', err.message);
    }
  } else {
    if (!fullName) {
      alert('Please enter your Full Name');
    } else if (!email) {
      alert('Please enter a valid Phinma Email Address');
    } else if (!isValidPhinmaEmail(email)) {
      alert('Please enter a valid Phinma Email Address ending with @phinmaed.com');
    } else if (!password) {
      alert('Please enter your Password');
    } else if (!confirmPassword) {
      alert('Please confirm your Password');
    } else if (!passwordsMatch) {
      alert('Password does not match');
    }
  }
};

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        setEmailVerified(user.emailVerified);
      } else {
        setEmailVerified(false);
      }
    });

    return () => unsubscribe();
  }, []);

  return (
    <View className="flex-1 bg-white" style={{ backgroundColor: themeColors.secondary }}>
      <SafeAreaView className="flex">
        <View className="flex-row justify-start">
        </View>
        <View className="flex-row justify-center">
          <Image source={require('../assets/images/Signup.png')} style={{ width: 305, height: 150 }} />
        </View>
      </SafeAreaView>
      <View style={{ borderTopLeftRadius: 50, borderTopRightRadius: 50 }} className="flex-1 bg-lime-100 px-8 pt-8">
        <View className="form space-y-2">
          <Text className="text-gray-700 ml-1">Full Name</Text>
          <TextInput
          className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
           value={fullName}
            onChangeText={value => setName(value.toUpperCase())} 
             placeholder='Enter Name'
              autoCapitalize="words"
                  />
          <Text className="text-gray-700 ml-1">Phinma Email</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
            value={email}
            onChangeText={value => setEmail(value)}
            placeholder='Enter Email'
          />
          <Text className="text-gray-700 ml-1">Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
            secureTextEntry={isSecureEntry}
            value={password}
            onChangeText={handlePasswordChange}
            placeholder='Enter Password'
          />
          <TouchableOpacity
            onPress={() => {
              setIsSecureEntry((prev) => !prev);
            }}
            style={{ position: 'absolute', right: 20, top: 250 }}
          >
            <Text>{isSecureEntry ? "Show" : "Hide"}</Text>
          </TouchableOpacity>
          <Text className="text-gray-700 ml-1">Confirm Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
            secureTextEntry={isSecureEntry}
            value={confirmPassword}
            onChangeText={handleConfirmPasswordChange}
            placeholder="Confirm Password"
          />
          <TouchableOpacity
            onPress={() => {
              setIsSecureEntry((prev) => !prev);
            }}
            style={{ position: 'absolute', right: 20, top: 370 }}
          >
            <Text>{isSecureEntry ? "Show" : "Hide"}</Text>
          </TouchableOpacity>
          {!passwordsMatch && (
            <Text className="text-red-500 ml-4">Password doesn't match</Text>
          )}
          {emailVerified && (
            <Text style={{ color: 'green', marginLeft: 10 }}>Email verified! You can proceed to the next step.</Text>
          )}
          <TouchableOpacity
            style={{ padding: 10, backgroundColor: '#70bb85', borderRadius: 9999, overflow: 'hidden' }}
            onPress={handleSubmit}
          >
            <Text className="text-xl font-bold text-center text-white">Sign Up</Text>
          </TouchableOpacity>
        </View>
        <View className="flex-row justify-center mt-7">
          <Text className="text-gray-500 font-semibold">Already have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={{ color: '#70bb85', fontWeight: 'bold' }}> Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}