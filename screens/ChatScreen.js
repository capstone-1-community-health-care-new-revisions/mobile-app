import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, BackHandler, TextInput, StyleSheet } from 'react-native';
import { collection, doc, getDoc, onSnapshot } from 'firebase/firestore';
import { firestore } from '../config/firebase';
import { getAuth } from 'firebase/auth';
import ChatInterface from './ChatInterface';
import { format } from 'date-fns';
import { MaterialCommunityIcons } from '@expo/vector-icons'; // Import notification icon

const ChatScreen = () => {
  const [selectedConversation, setSelectedConversation] = useState(null);
  const [admins, setAdmins] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredAdmins, setFilteredAdmins] = useState([]);
  const [lastMessages, setLastMessages] = useState({});

  const auth = getAuth();

  useEffect(() => {
    const backAction = () => {
      if (selectedConversation) {
        setSelectedConversation(null);
        setSearchQuery('');
        return true;
      }
      return false;
    };

    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

    return () => backHandler.remove();
  }, [selectedConversation]);

  useEffect(() => {
    const user = auth.currentUser;

    const fetchAdmins = () => {
      try {
        const adminsRef = collection(firestore, 'admins');
        const unsubscribe = onSnapshot(adminsRef, (querySnapshot) => {
          const adminsData = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data()
          }));
          setAdmins(adminsData);
        });
        return unsubscribe;
      } catch (error) {
        console.error('Error fetching admins:', error);
      }
    };

    const fetchCurrentUser = async () => {
      try {
        const userDocRef = doc(firestore, 'users', user.uid);
        const userDocSnap = await getDoc(userDocRef);
        if (userDocSnap.exists()) {
          const userData = userDocSnap.data();
          setCurrentUser({ ...userData, id: user.uid });
        } else {
          console.error('Current user data not found');
        }
      } catch (error) {
        console.error('Error fetching current user:', error);
      }
    };

    if (user) {
      const unsubscribeAdmins = fetchAdmins();
      fetchCurrentUser();
      return () => {
        unsubscribeAdmins();
      };
    }
  }, [auth]);

  useEffect(() => {
    // Filter admins based on search query
    const filtered = admins.filter(
      (admin) =>
        admin.displayName.toLowerCase().includes(searchQuery.toLowerCase()) ||
        admin.specialty.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setFilteredAdmins(filtered);
  }, [searchQuery, admins]);

  useEffect(() => {
    // Listen for changes in conversations and update last message
    const unsubscribe = () => {
      const unsubscribeListeners = filteredAdmins.map((admin) => {
        const conversationId = generateConversationId(auth.currentUser.uid, admin.id);
        const conversationRef = doc(firestore, 'conversations', conversationId);
        return onSnapshot(conversationRef, (doc) => {
          if (doc.exists()) {
            const lastMessage = doc.data().lastMessage;
            setLastMessages((prev) => ({
              ...prev,
              [admin.id]: lastMessage
            }));
          }
        });
      });
      return () => {
        unsubscribeListeners.forEach((unsubscribeListener) => {
          unsubscribeListener();
        });
      };
    };

    return unsubscribe();
  }, [filteredAdmins]);

  const handleAdminPress = (admin) => {
    const conversationId = generateConversationId(auth.currentUser.uid, admin.id);
    setSelectedConversation({ ...admin, conversationId });
    setSearchQuery('');
  };

  const generateConversationId = (userId1, userId2) => {
    const sortedIds = [userId1, userId2].sort();
    return sortedIds.join('_');
  };

  // Function to sort admins based on the presence of last message and its timestamp
  const sortAdmins = (a, b) => {
    const hasLastMessageA = !!lastMessages[a.id] && lastMessages[a.id].timestamp;
    const hasLastMessageB = !!lastMessages[b.id] && lastMessages[b.id].timestamp;

    if (hasLastMessageA && hasLastMessageB) {
      const timestampA = lastMessages[a.id].timestamp.toDate();
      const timestampB = lastMessages[b.id].timestamp.toDate();
      return timestampB - timestampA;
    } else if (hasLastMessageA && !hasLastMessageB) {
      return -1; // Admin A has last message, so it should come first
    } else if (!hasLastMessageA && hasLastMessageB) {
      return 1; // Admin B has last message, so it should come first
    } else {
      return 0; // Both admins have no last message, maintain original order
    }
  };

  return (
    <View style={styles.container}>
      {selectedConversation ? (
        <ChatInterface conversation={selectedConversation} currentUser={currentUser} />
      ) : (
        <>
          <Text style={styles.header}>Conversation List</Text>
          <TextInput
            style={styles.searchInput}
            placeholder="Search for a nurse and physician name"
            onChangeText={setSearchQuery}
            value={searchQuery}
          />
          <View style={styles.separator} />
          <View style={styles.scrollViewContainer}>
            <ScrollView style={styles.scrollView}>
              {filteredAdmins.sort(sortAdmins).map((item) => (
                <TouchableOpacity key={item.id} onPress={() => handleAdminPress(item)}>
                  <View style={styles.conversationContainer}>
                    <Image source={{ uri: item.photoURL }} style={styles.avatar} />
                    <View style={styles.textContainer}>
                      <View style={styles.messageContainer}>
                        <Text style={styles.displayName}>{item.displayName}</Text>
                        <Text>{item.specialty}</Text>
                      </View>
                      {lastMessages[item.id] && lastMessages[item.id].timestamp && (
                        <View style={styles.lastMessageContainer}>
                          <Text style={{ color: lastMessages[item.id].senderId === currentUser.id ? '#555' : 'black' }}>
                            {lastMessages[item.id].senderId === currentUser.id ? 'You' : lastMessages[item.id].displayName}:
                            {lastMessages[item.id].text.length > 15
                              ? `${lastMessages[item.id].text.substring(0, 15)}...`
                              : lastMessages[item.id].text}
                          </Text>
                          <Text style={{ color: lastMessages[item.id].senderId === currentUser.id ? '#555' : 'black' }}>
                            {format(lastMessages[item.id].timestamp.toDate(), 'h:mm aa')}
                          </Text>
                        </View>
                      )}
                    </View>
                    {lastMessages[item.id] && lastMessages[item.id].timestamp && (
                      <View style={styles.notificationIconContainer}>
                        {lastMessages[item.id].senderId !== currentUser.id && (
                          <MaterialCommunityIcons name="bell" size={15} color="red" />
                        )}
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(173, 216, 230, 0.945)',
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    marginVertical: 10,
    textAlign: 'center',
    marginTop: 50,
  },
  searchInput: {
    height: 40,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    marginBottom: 10,
  },
  scrollViewContainer: {
    height: 580,
    marginHorizontal: 20,
  },
  scrollView: {
    flex: 1,
  },
  conversationContainer: {
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    marginBottom: 10,
    borderRadius: 10,
    position: 'relative', // Ensure the position of the notification icon container
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  textContainer: {
    flex: 1,
  },
  displayName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  messageContainer: {
    flex: 1,
  },
  lastMessageContainer: {
    flexShrink: 1,
    alignItems: 'flex-end',
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    marginHorizontal: 20,
    marginBottom: 10,
  },
  notificationIconContainer: {
    position: 'absolute',
    top: 5,
    right: 5,
    padding: 5,
  },
});

export default ChatScreen;
